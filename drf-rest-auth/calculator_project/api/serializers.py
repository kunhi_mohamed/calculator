from rest_framework import serializers
from calculator import models


class ComputationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'title',
            'description',
            'operations'
        )
        model = models.Computation