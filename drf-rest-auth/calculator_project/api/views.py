from rest_framework import generics

from calculator import models
from . import serializers


class ListComputation(generics.ListCreateAPIView):
    queryset = models.Computation.objects.all()
    serializer_class = serializers.ComputationSerializer


class DetailComputation(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Computation.objects.all()
    serializer_class = serializers.ComputationSerializer