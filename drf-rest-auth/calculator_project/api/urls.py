from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.ListComputation.as_view()),
    path('<int:pk>/', views.DetailComputation.as_view()),
    # path('rest-auth/', include('rest_auth.urls')),
]