# Generated by Django 2.1.4 on 2018-12-06 06:23

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('calculator', '0006_operationshistory_datecreated'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operationshistory',
            name='dateCreated',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 6, 6, 23, 43, 505669, tzinfo=utc), verbose_name='Created Time'),
        ),
    ]
