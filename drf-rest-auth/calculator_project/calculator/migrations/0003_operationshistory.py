# Generated by Django 2.1.4 on 2018-12-06 05:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calculator', '0002_computation_operations'),
    ]

    operations = [
        migrations.CreateModel(
            name='OperationsHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('operationString', models.TextField(default='test')),
                ('date', models.DateField(auto_now=True)),
            ],
        ),
    ]
