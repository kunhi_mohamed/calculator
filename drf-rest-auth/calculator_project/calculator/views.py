import json
import xlwt
import os
from io import BytesIO
from datetime import datetime
from django.conf import settings	
from django.shortcuts import render
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseNotFound

from .models import Computation, OperationsHistory
from django.http import JsonResponse


# Create your views here.


def home(request):
	return render(request,'dappx/index.html')

def operationsSet(request):
	requiredObject = Computation.objects.get(title="Operations")
	requiredDict = eval(requiredObject.operations)
	return JsonResponse(requiredDict)

def triggeroperations(request):
	requiredObject = Computation.objects.get(title="Operations")
	requiredDict = eval(requiredObject.operations)
	print(requiredDict)
	# requiredDict = {'-': '+', '/': '/', '+': '+', '*': '*'}
	templateDict = {}
	for each in requiredDict:
		if each == "-":
			templateDict["subtraction"] = requiredDict[each]
		elif each == "+":
			templateDict["addition"] = requiredDict[each]
		elif each == "*":
			templateDict["multiplication"] = requiredDict[each]
		elif each == "/":
			templateDict["division"] = requiredDict[each]

	return render(request,'dappx/trigger.html', templateDict)	

def triggeroperationUpdate(request):
	try:
		theupdatedOperationSet = request.POST.get('updateddata', None)
		if theupdatedOperationSet:
			theupdatedOperationSet = eval(theupdatedOperationSet)
			requiredObject = Computation.objects.get(title="Operations")
			requiredObject.operations = theupdatedOperationSet
			requiredObject.save()
			return JsonResponse(theupdatedOperationSet)

		return JsonResponse({})
	except Exception as e:
		print(str(e))		

@csrf_exempt
def saveOperationsHistory(request):
	try:
		theCurrentOperation = request.POST.get('content', None)
		if theCurrentOperation:
			operationDone = eval(theCurrentOperation)
			x = OperationsHistory()
			x.operationString = operationDone["content"]
			x.save()
		return JsonResponse({})
	except Exception as e:
		print(str(e))


def generate_excel_data(request):
	# content-type of response
	try:
		theDateDetails = request.POST.get('datedetails', None)
		if theDateDetails:
			theDateDetails = eval(theDateDetails)
			datetime_object_from = datetime.strptime(theDateDetails["from"], "%d-%m-%Y")
			datetime_object_to = datetime.strptime(theDateDetails["to"], "%d-%m-%Y")
			requiredObjects = OperationsHistory.objects.filter(dateCreated__range=(datetime_object_from, datetime_object_to))
			# requiredObjects = OperationsHistory.objects.filter(Q(dateCreated >= datetime_object_from, dateCreated <= datetime_object_to))
			if requiredObjects:
				response = HttpResponse(content_type='application/ms-excel')

				#decide file name
				response['Content-Disposition'] = 'attachment; filename="ThePythonDjango.xls"'

				#creating workbook
				stream = BytesIO()
				wb = xlwt.Workbook(encoding='utf-8')

				#adding sheet
				ws = wb.add_sheet("sheet1")

				# Sheet header, first row
				row_num = 0

				font_style = xlwt.XFStyle()
				# headers are bold
				font_style.font.bold = True

				#column header names, you can use your own headers here
				columns = ['Operations', 'Date']

				#write column headers in sheet
				for col_num in range(len(columns)):
					ws.write(row_num, col_num, columns[col_num], font_style)

				# Sheet body, remaining rows
				font_style = xlwt.XFStyle()

				#get your data, from database or from a text file...
				data = requiredObjects #dummy method to fetch data.
				for my_row in data:
					row_num = row_num + 1
					ws.write(row_num, 0, my_row.operationString, font_style)
					ws.write(row_num, 1, str(my_row.dateCreated.date()), font_style)

				wb.save(stream)
				fs = FileSystemStorage(location="media/") #defaults to   MEDIA_ROOT  
				filename = fs.save("operationsHistory.xls", stream)
				file_url = fs.url(filename)
				return JsonResponse({"file_Url": file_url, "status":"data"})
			else:
				return JsonResponse({"status":"no data"})
	except Exception as e:
		print(str(e))


def download_excel_data(request, name):
	try:
		file_path = settings.MEDIA_DIR+"/"+name
		with open(file_path, 'rb') as fh:
			response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
			response['Content-Disposition'] = 'inline; filename=' + name
			return response
	except Exception as e:
		print(str(e))			
	# response = HttpResponse(content_type='application/ms-excel')

	# #decide file name
	# response['Content-Disposition'] = 'attachment; filename="ThePythonDjango.xls"'

	# #creating workbook
	# wb = xlwt.Workbook(encoding='utf-8')

	# #adding sheet
	# ws = wb.add_sheet("sheet1")

	# # Sheet header, first row
	# row_num = 0

	# font_style = xlwt.XFStyle()
	# # headers are bold
	# font_style.font.bold = True

	# #column header names, you can use your own headers here
	# columns = ['Column 1', 'Column 2', 'Column 3', 'Column 4', ]

	# #write column headers in sheet
	# for col_num in range(len(columns)):
	# 	ws.write(row_num, col_num, columns[col_num], font_style)

	# # Sheet body, remaining rows
	# font_style = xlwt.XFStyle()

	# #get your data, from database or from a text file...
	# data = [1,2,3,4,5,6] #dummy method to fetch data.
	# for my_row in data:
	# 	row_num = row_num + 1
	# 	ws.write(row_num, 0, my_row, font_style)
	# 	ws.write(row_num, 1, my_row, font_style)
	# 	ws.write(row_num, 2, my_row, font_style)
	# 	ws.write(row_num, 3, my_row, font_style)

	# wb.save(response)
	# return response					