from django.db import models
from django.utils import timezone

class Computation(models.Model):
	title = models.CharField(max_length=200)
	description = models.TextField()
	operations = models.TextField(default="test")

	def __str__(self):
		"""A string representation of the model."""
		return self.title


class OperationsHistory(models.Model):
	operationString = models.TextField(default="test")
	dateCreated = models.DateTimeField('Created Time', default=timezone.now())

	def __str__(self):
		"""A string representation of the model."""
		return self.operationString		