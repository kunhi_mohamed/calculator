from django.contrib import admin

from .models import Computation, OperationsHistory

admin.site.register(Computation)

admin.site.register(OperationsHistory)